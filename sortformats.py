#!/usr/bin/env python3

"""Sort a list of formats according to their level of compression"""

import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """
    return fordered.index(format1) < fordered.index(format2)

def find_lower_pos(formats: list, pivot: int) -> int:
    """Find the lower format in formats after pivot
    Returns the index of the lower format found"""
    min_index = pivot
    for i in range(pivot + 1, len(formats)):
        if lower_than(formats[i], formats[min_index]):
            min_index = i
    return min_index

def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered"""
    for i in range(len(formats)):
        min_index = find_lower_pos(formats, i)
        if i != min_index:
            formats[i], formats[min_index] = formats[min_index], formats[i]
    return formats

def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end=" ")
    print()

if __name__ == '__main__':
    main()
